package fr.onepoint.universaltester;


import fr.onepoint.test.resource.ResourceExtension;
import fr.onepoint.test.resource.ResourceFile;
import fr.onepoint.universaltester.executor.Reporter;
import fr.onepoint.universaltester.executor.Runner;
import fr.onepoint.universaltester.executor.Tester;
import fr.onepoint.universaltester.results.Result;
import fr.onepoint.universaltester.ssh.SSH;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExtendWith(ResourceExtension.class)
class TestCaseBuilderTest {

    private static final Reporter DUMMY_REPORTER = (differences, config) -> {};
    private static final Runner DUMMY_RUNNER = (config, ssh) -> {};

    private static final Tester DUMMY_TESTER = new Tester() {
        @Override
        public Map<String, String> prepareFiles(Config config, SSH ssh) {
            return new HashMap<>();
        }

        @Override
        public List<Result> compare(Path reference, Path toCompare, Config config) {
            return new ArrayList<>();
        }
    };

    @ResourceFile("dummyConfig.properties")
    private Path dummyConfig;

    @Test
    void it_should_build_if_all_paramaters_are_setted(){
        //Given a nominal case
        TestCaseBuilder builder = new TestCaseBuilder()
                .addReporter(DUMMY_REPORTER)
                .addTester(DUMMY_TESTER)
                .addRunner(DUMMY_RUNNER)
                .addConfig(dummyConfig);

        //When TestCaseBuilder.build is called
        TestCase testCase = builder.build();

        //Then a testCase should be returned
        Assertions.assertThat(testCase).isNotNull();
    }

}

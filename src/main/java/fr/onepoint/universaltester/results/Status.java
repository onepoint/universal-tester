package fr.onepoint.universaltester.results;

public enum Status {
    OK,
    KO,
    ERROR
}

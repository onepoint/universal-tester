package fr.onepoint.universaltester.ssh;

import com.jcraft.jsch.*;
import fr.onepoint.universaltester.UniversalTesterException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.text.MessageFormat.format;

public class CloseableSSH implements Closeable, SSH {

    private static final Logger LOGGER = LoggerFactory.getLogger(SSH.class);

    private final Session session;

    public CloseableSSH(String host, String user, String password, int port) {
        LOGGER.debug("Creating new SSH connection to {}@{}:{}", user, host, port);
        JSch jsch = new JSch();
        try {
            session = jsch.getSession(user, host, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(30000);
        } catch (JSchException e) {
            throw new UniversalTesterException(e);
        }

    }

    @Override
    public String exec(String command) {
        LOGGER.debug("Executing command {} to {}", command, session.getHost());
        ChannelExec channel = null;
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);
            channel.setInputStream(null);
            channel.connect();
            return IOUtils.toString(channel.getInputStream(), StandardCharsets.UTF_8);
        } catch (JSchException | IOException e) {
            LOGGER.error("Fail to execute command {} on {}", command, session.getHost());
            throw new UniversalTesterException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    @Override
    public Path downloadFile(String distantLocation, Path targetDir) {
        createDirIfNotExists(targetDir);
        LOGGER.debug("Downloading file {} to {}", distantLocation, session.getHost());
        ChannelSftp channel = null;
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.setInputStream(null);
            channel.connect();
            Path target = targetDir.resolve(FilenameUtils.getName(channel.realpath(distantLocation)));
            channel.get(distantLocation, target.toString());
            LOGGER.debug("File {} downloaded", target);
            return target;
        } catch (JSchException | SftpException e) {
            LOGGER.error("Failed to download file {}", distantLocation);
            throw new UniversalTesterException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    private void createDirIfNotExists(Path targetDir) {
        File targetDirFile = targetDir.toFile();
        if(!targetDirFile.exists() && !targetDirFile.mkdirs()){
            throw new UncheckedIOException(new IOException(format("Fail to create directory {0}", targetDir)));
        }
    }

    @Override
    public void upload(Path toUpload, String distantRepo) {
        try (InputStream inputStream = Files.newInputStream(toUpload)) {
            this.upload(inputStream, FilenameUtils.getName(toUpload.toString()), distantRepo);
        } catch (IOException e) {
            throw new UniversalTesterException(e);
        }
    }

    private void upload(InputStream toUpload, String filename, String distantRepo) {
        LOGGER.debug("Upload input stream to {}:{}", session.getHost(), distantRepo);
        ChannelSftp channel = null;
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.setInputStream(null);
            channel.connect();
            String target = distantRepo + filename;
            channel.put(toUpload, target);
            LOGGER.debug("File uploaded to {}", target);
        } catch (JSchException | SftpException e) {
            LOGGER.error("Failed to upload file {}", filename);
            throw new UniversalTesterException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    @Override
    public void close() {
        if (session != null && session.isConnected()) {
            session.disconnect();
        }
    }
}

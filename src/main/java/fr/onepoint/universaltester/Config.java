package fr.onepoint.universaltester;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private Map<String, String> properties;

    public Config(){
        properties = new HashMap<>();
    }

    public void addProperties(Path configPath) {
        try (
                FileInputStream fis = new FileInputStream(configPath.toFile());
                InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8)
        ) {
            Properties prop = new Properties();
            prop.load(inputStreamReader);
            prop.forEach((key, value) -> properties.put((String) key, (String) value));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String getProperty(String key){
        return properties.get(key);
    }

    public Map<String, String> getProperties(){
        return Collections.unmodifiableMap(properties);
    }

    public void addProperty(String name, String value){
        LOGGER.debug("property {} added with value {}", name, value);
        properties.put(name, value);
    }

    public void addConfig(Config config){
        config.getProperties().forEach(this::addProperty);
    }
}

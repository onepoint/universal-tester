﻿# Universal tester

An API providing tools to run tests on distant applications via SSH.
With this API you can create different tests case using the `TestCaseBuilder` composed with Runners corresponding to a step in a run of your application, and then launch tests with Testers and finally call Reporters to interprete the results created by Testers.
Your `TestCase` will always run the same way, `Runners -> Testers -> Reporters`.
It splits theses modules in three lists, so it doesn't matter the order you add types of modules, but it does the order you add them by group.

For example : 
```Java
TestCaseBuilder builder = new TestCaseBuilder();
builder.addRunner(new Runner1())
		.addReporter(new Reporter1())
		.addTester(new Tester1())
		.addRunner(new Runner2());
```

This example will run Runner1 -> Runner2 -> Tester1 -> Reporter1

## TestCase

The `TestCase` class is the direct representation of your test case (how it has to run your application, what should be tested, how the results should be displayed...).

It contains a `Config` object based on `.properties` files to manipulate variables in your code.

To create a new `TestCase`, it is recommended to use the `TestCaseBuilder` because it does more controls than `TestCase` mostly on the mandatory variables in the `Config`.

When the method `test()` is called, it will run your case in 3 steps, first the Runners, then your Testers and finally your Reporters.

## TestCaseBuilder

As mentionned, this class helps you to build your `TestCase`. You can add three types to your `TestCaseBuilder` :

1. Runners
2. Testers
3. Reporters

Here an example on how to build your test case :
```Java
TestCaseBuilder builder = new TestCaseBuilder();
builder.addReporter(...)
		.addTester(...)
		.addRunner(...);
		.build();
```
If you add `Tester`, `Reporter` or `Runner` implementing the interface `HasMandatory`, it will check when building if all variables in all mandatories are presents in the Config.

## HasMandatoryProps

Implementing this interface in all your modules allows the `TestCaseBuilder` to test if all variables used by them are presents in the configuration based on your properties files.

## Runners

Runners can split the execution of your application (ex: Begin/Process/End). They simulate a run in your application to set up your database or generate files.

## Testers

When you first run this tool, you have no reference file to do comparison between new generated files and old ones, so the first time, it will just do a copy of this new files and add prefix `ref_` to the filename for the next times.

Testers are launched after the runners and are divided in two steps:
 First it prepares files, returning the path on the server and then compare the reference file with the new one and return a list of `Result` that will be interpreted by Reporters. 

It can produce two types of  `Result`, `Identical` if the two files are identicals or `Difference` if it founds some differences.

## Reporters 

Reporters represents the end of the test case, it will interpret Results created by Testers. 

Some additionnal methods can be implemented :

```java
beforeCaseSuite()
beforeCase(Config config)
afterCaseSuite()
```

The method `beforeCaseSuite` is runned once before all cases. `afterCaseSuite` is runned once after all cases.
The method `beforeCase` is runned before testCase.




## Config

You have to create a `.properties` file and add it to your `TestCaseBuilder` to declare variables about SSH and the local directory where this tool will save and compare files :
```properties
ssh.host=
ssh.port=
ssh.user=
ssh.password=
assertions.directory=
assertions.casename=
```
Some modules can require additionnal properties, but this is the minimum needed.


# Documentation

Documentation can be found [here](https://onepoint.gitlab.io/universal-tester/)

# Licence
[MIT License](http://www.opensource.org/licenses/mit-license.php)
